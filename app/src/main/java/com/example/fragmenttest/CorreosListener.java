package com.example.fragmenttest;

public interface CorreosListener
{
    void onCorreoSeleccionado(Correo c);
}